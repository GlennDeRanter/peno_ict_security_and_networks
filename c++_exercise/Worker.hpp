#include <iostream>
#include "Task.hpp"
#include <queue>
#include <thread>
#include <mutex>
#include <random>
#include <algorithm>
#include <condition_variable>
using namespace std;

#include <iostream>
#include "Task.hpp"
#include <queue>
#include <thread>
#include <mutex>
#include <random>
#include <algorithm>
#include <condition_variable>
using namespace std;

#ifndef Worker_HPP
#define Worker_HPP


    class Worker
    {
    public:
        typedef Task task_type;
        typedef int Size_type;
        typedef char name_type;
    public:
        //The constructor
        Worker(queue<Task> queue)
        :tasksQueue(queue)
        {}

        //Copy constructorr
        Worker(Worker const& work) 
       :tasksQueue(work.tasksQueue)
        {}

        //Assignment operator
        Worker &operator=(Worker &&) = default;

        // //The destructor
        // ~Worker();

        //Function to get the taskname 
        float getTotalWaitingTime() {return totalWaitingTime;}  

        //The function doWork is the basis function when you want to execute the tasks
        // this can either be done as a FIFO or as SJF (the default is FIFO)
        inline void doWork(bool SJF=false){
            if (SJF == true){
                std::cout << "Using SJF" << std::endl;
                sortSJF();
            }
            else{
                std::cout << "using FIFO" << std::endl;
            }
            int waitingTimeOne=0;
            int totalWaitingTime = 0 ;
            int numberOfTasks = 0;
            while(!tasksQueue.empty()){
                Task processedTask = tasksQueue.front();
                waitingTimeOne = processedTask.getRequiredTime() + waitingTimeOne;
                totalWaitingTime = totalWaitingTime + waitingTimeOne;
                numberOfTasks = numberOfTasks + 1;
                tasksQueue.pop();
                processedTask.doJob();
	            std::cout << "Processed task: " << processedTask.getTaskName() << " for " << processedTask.getRequiredTime() << "s" << std::endl;
            }
            totalWaitingTime = totalWaitingTime - waitingTimeOne;
            double averageTimeWaiting = totalWaitingTime/numberOfTasks;
            std::cout << "The worker is stopped! The average waiting time is: " << averageTimeWaiting << "s" << std::endl;
        }

       //Function that sorts the queue in a SJF fashion
       inline void sortSJF() {
            std::vector<Task> temporaryVector;
            while (!tasksQueue.empty()){
                Task task = tasksQueue.front();
                tasksQueue.pop();
                temporaryVector.push_back(task); 
            }
            std::sort(temporaryVector.begin(), temporaryVector.end(), [](Task a, Task b) {
            return a.getRequiredTime() > b.getRequiredTime();
            });
    	    while (!temporaryVector.empty()){
                Task task = temporaryVector.back();
                temporaryVector.pop_back();
                tasksQueue.push(task);
            }
       }

        //Function that executes all tasks in the queue and waits for new taskss
        inline void doWorkMultithread(bool SJF=false){
            if (SJF) {
                std::cout << "The worker is using SJF" << std::endl;
            } else {
                std::cout << "The worker is using FIFO" << std::endl;
            }
            while (!finished || !tasksQueue.empty()){
    	        std::unique_lock<std::mutex> lck(m);
                cv.wait(lck, [&]{ return !tasksQueue.empty()||finished; });
                if (finished) break;
                if (SJF) sortSJF();
	            Task processedTask = tasksQueue.front();
	            tasksQueue.pop();
                totalWaitingTime = totalWaitingTime + processedTask.getWaitingTime();
                name_type * taskName = processedTask.getTaskName();
	            std::cout << "Task processed by worker: name=";
                for (int i = 0; i < 16; i++) {
                    std::cout << taskName[i];
                }   
                std::cout << " (" << processedTask.getRequiredTime() << "s)" << std::endl;                
                m.unlock();
                cv.notify_all();
                processedTask.doJob();
            }
            std::cout << "The worker thread is finished and so stopped!" << std::endl;
        }

        //Function makes the doWorkMultithread thread return after doing the last task in the queue
        inline void closeQueue(){
            std::unique_lock<std::mutex> lck(m);
            while (!(tasksQueue.empty()&&(threads==3))) cv.wait(lck);
            finished = true;
            std::cout << "The average queue waiting time for the tasks was: " << totalWaitingTime/12 << "s" << std::endl;
            std::cout << "The submission and processing of all tasks is set to be finished!" << std::endl;
            m.unlock();
            cv.notify_all();
        }

        //Generation of tasks and added to the queue of the worker in a different threads (created in the main thread) 
        inline void addTasks(int numberOfTasks, int id){
            int r;
            for (int i=0; i<numberOfTasks; ++i){
                char taskName[16] = {0};
                for(unsigned int i = 0; i < 16; ++i){
                    r = rand() %26;
                    taskName[i] = 'a' + r;
                }
                //Generate random task duration between 1 and 3s
                int taskDuration = rand() % 3 + 1;
                Task newTask = Task(taskDuration, taskName);
                m.lock();
                tasksQueue.push(newTask); 
                std::cout << "TREAD " << id << " --> " << "task " << i+1 << " added to the queue: name=" << taskName << " with duration of " << taskDuration << "s" << std::endl;
                m.unlock();
                cv.notify_all();

            }
            threads = threads + 1;  
        }
        
    private:
        Size_type maxQueueSize;
        std::queue<Task> tasksQueue;
        bool finished = false;
        std::mutex m;
        std::condition_variable cv;
        int threads = 0;
        float totalWaitingTime = 0;
   
    };


#endif 