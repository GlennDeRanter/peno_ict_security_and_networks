#include <unistd.h>
//#include "windows.h" 
#include <iostream>
#include <chrono> 
#include <ctime>

using namespace std;
#ifndef Task_HPP
#define Task_HPP


class Task
{
public:
    //Type definitions for the class
    typedef int time_type;
    typedef char name_type;

public:
    //Place in the header for the constructor
    Task(time_type time, string name)
        :requiredTime(time)
        ,taskName(new name_type[16])
        {
            for (int i =0; i <16; i++) taskName[i] = name[i];
            startTime = std::chrono::system_clock::now();
        }
    
    //Place in the header for the destructor
    ~Task(){
        delete [] taskName;
    }
    
    //Place in the header for the copy constructor
    Task(Task const& obj)
        :requiredTime(obj.requiredTime)
        ,taskName(new name_type[16])
        ,startTime(obj.startTime)
        {
            for(int i = 0; i<16;++i) taskName[i] = obj.taskName[i];
        } 


    //Place in the header for the assignment operator
    Task& operator=(Task const& obj){
        bool identical = true;
        for(int i = 0; i<16;++i) {
            if (taskName[i] != obj.taskName[i]) {
                identical = false;
                break;
            }
        }
        if ((requiredTime != obj.getRequiredTime() && !identical)) {
            requiredTime = obj.getRequiredTime();
            startTime = obj.startTime;
            for(int i = 0; i<16;++i) taskName[i] = obj.taskName[i];
        }
        return *this;
    }
    
    inline bool operator<( const Task& other) {
          return getRequiredTime() < other.getRequiredTime();
   }

    public:
    //Function to get the required time needed to execute the task 
        time_type getRequiredTime() const {return requiredTime;}

    //Function to get the taskname 
        name_type* getTaskName() const {return taskName; }    

    //Function to get the taskname 
        auto getStartTime() {return startTime;}  

    //Function to set the required time needed to execute the task
        time_type setRequiredTime(time_type time) {requiredTime = time; return requiredTime;}
    
    //Function to set the name of the task
        name_type * setTaskName(name_type *name) {
                for (int i=0; i<16; i++){
                    taskName[i] = name[i];
                }
                return taskName;}

    //Header for the doJob function
        inline void doJob(void) {
            for (int i = 0; i < 15; i++) {
                std::cout << taskName[i];
            }   
            std::cout<< taskName[15]<< std::endl;
            sleep(requiredTime);
        }
   
    //Return the waiting time of the task
        inline float getWaitingTime() {
            std::chrono::duration<float> duration = std::chrono::high_resolution_clock::now() - getStartTime();
            return duration.count();
        }
    
    private:
    // The time the worker is occupied when the task is executed
        time_type requiredTime; 

    // The name of the task
        name_type* taskName;  

    // The start time of the task
       std::chrono::time_point<std::chrono::system_clock> startTime;
};
#endif

