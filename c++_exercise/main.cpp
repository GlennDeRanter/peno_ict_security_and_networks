#include "Task.hpp"
#include "Worker.hpp"

#include <thread>
#include <iostream>
#include <thread>
#include <pthread.h>
#include <stdio.h>

int main(){
    // Task v(5, "hellohellohelloh");
    // std::cout << "Task is created" << std::endl;

    // int time;
    // time = v.getRequiredTime();

    // std::cout << "time =" << time << std::endl;
    // string name;
    // name = v.getTaskName();

    // std::cout << "name =" << name << std::endl;

    // Task x(v);

    // x.setRequiredTime(2);
    
    // std::cout << "Time has been changed" << std::endl;

    // time = x.getRequiredTime();

    // std::cout << "time =" << time << std::endl;
    
    // x.setTaskName("goodgoodgoodgood");

    // std::cout << "Name has been changed" << std::endl;

    // name = x.getTaskName();

    // std::cout << "name=" << name << std::endl;

    // v.doJob();
    // std::cout << "job done" << std::endl;

    // Task task1 = Task(3, "treetreetreetree");
    // Task task2 = Task(5, "appleappleapplea");
    // Task task3 = Task(5, "balbalbalbalbalb");
    // queue<Task> queue1;
    // queue1.push(task1);
    // queue1.push(task2);
    // queue1.push(v);
    // queue1.push(x);

    // Worker workster = Worker(queue1);

    // queue1.push(task3); 
    // Worker workie = Worker(queue1);

    // workster.doWork(false);

    // workie.doWork(true);

    // //Create the thread that consumes tasks using SJF(=true)
    // std::cout << " " << std::endl;
    // std::cout << " " << std::endl;
    // Worker* w = new Worker({});
    // std::thread worker(&Worker::doWorkMultithread, w, true);

    // //Stop the worker thread when all tasks are submitted and processed (thread)
    // std::thread stop(&Worker::closeQueue, w);

    // //Create three task producing threads
    // std::thread taskThreads[3];
    // for (int i=0; i<3; ++i) {
    //     taskThreads[i] = std::thread(&Worker::addTasks, w, 2, i+1);
    // }

    // //Activate all threads
    // stop.join();
    // worker.join();
    // taskThreads[0].join();
    // taskThreads[1].join();
    // taskThreads[2].join();
    
    // return 0;
    string message;
    std::cout << "give a message";
    std::cin >> message;
    std::cout << message << std::endl;
};